package org.marketplace.view.comments.model.error;

import org.marketplace.view.comments.model.response.GetCommentListResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommentListError extends GetCommentListResponse {
  private String message;
}
