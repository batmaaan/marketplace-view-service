package org.marketplace.view.comments.model.error;

import org.marketplace.view.comments.model.response.GetCommentResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommentByIdError extends GetCommentResponse {
private String message;
}
