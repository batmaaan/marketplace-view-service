package org.marketplace.view.comments.model.error;

import org.marketplace.view.comments.model.response.SaveCommentResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SaveCommentError extends SaveCommentResponse {
private String message;
}
