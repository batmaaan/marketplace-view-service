package org.marketplace.view.comments.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.marketplace.comment.model.Comment;
import org.marketplace.comment.model.response.CommentListResponse;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GetCommentListResponse extends CommentListResponse {
  private String status;
  private String description;
  private boolean hasNext;
  private List<Comment> comments;
}
