package org.marketplace.view.comments.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.marketplace.comment.model.response.DeleteResponse;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DeleteCommentResponse extends DeleteResponse {
  private String status;
  private String description;
}
