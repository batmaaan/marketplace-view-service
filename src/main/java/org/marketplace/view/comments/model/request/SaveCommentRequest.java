package org.marketplace.view.comments.model.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.marketplace.comment.model.request.CommentRequest;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SaveCommentRequest extends CommentRequest {
  private Integer productId;
  private String text;
}
