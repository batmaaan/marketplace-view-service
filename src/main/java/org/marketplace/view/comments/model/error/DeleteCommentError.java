package org.marketplace.view.comments.model.error;

import org.marketplace.view.comments.model.response.DeleteCommentResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DeleteCommentError extends DeleteCommentResponse {
  private String message;
}
