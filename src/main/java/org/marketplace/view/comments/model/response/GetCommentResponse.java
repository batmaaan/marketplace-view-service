package org.marketplace.view.comments.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.marketplace.comment.model.Comment;
import org.marketplace.comment.model.response.CommentResponse;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class GetCommentResponse extends CommentResponse {
  private String status;
  private String description;
  private Comment comment;
}
