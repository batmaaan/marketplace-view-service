package org.marketplace.view.comments.comment;

import org.marketplace.view.comments.model.request.SaveCommentRequest;
import org.marketplace.view.comments.model.response.DeleteCommentResponse;
import org.marketplace.view.comments.model.response.GetCommentListResponse;
import org.marketplace.view.comments.model.response.SaveCommentResponse;
import org.marketplace.view.jwt.AppUserDetails;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.marketplace.comment.model.utils.Order;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;

@Slf4j
@Controller
@AllArgsConstructor
public class CommentController {


  private final CommentService commentService;


  @QueryMapping
  public GetCommentListResponse getCommentsByUser(@AuthenticationPrincipal AppUserDetails user,
                                                  @Argument int limit,
                                                  @Argument int offset,
                                                  @Argument Order order) {
    return commentService.getCommentsByUser(user.getUser().getId(), limit, offset, order);
  }

  @QueryMapping
  public GetCommentListResponse getCommentsByProduct(@Argument Integer productId,
                                                     @Argument int limit,
                                                     @Argument int offset,
                                                     @Argument Order order) {
    return commentService.getCommentsByProduct(productId, limit, offset, order);
  }

  @MutationMapping
  public SaveCommentResponse saveComment(@AuthenticationPrincipal AppUserDetails user,
                                         @Argument SaveCommentRequest commentRequest) {

    return commentService.saveComment(user.getUser().getId(), commentRequest);

  }

  @QueryMapping
  public DeleteCommentResponse deleteCommentById(@AuthenticationPrincipal AppUserDetails user,
                                                 @Argument Integer id,
                                                 @Argument Integer productId) {
    return commentService.deleteCommentById(id, productId, user.getUser().getId());
  }
}

