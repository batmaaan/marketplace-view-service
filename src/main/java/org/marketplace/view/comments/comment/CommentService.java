package org.marketplace.view.comments.comment;

import org.marketplace.view.comments.model.error.CommentListError;
import org.marketplace.view.comments.model.error.DeleteCommentError;
import org.marketplace.view.comments.model.error.SaveCommentError;
import org.marketplace.view.comments.model.response.DeleteCommentResponse;
import org.marketplace.view.comments.model.response.GetCommentListResponse;
import org.marketplace.view.comments.model.response.SaveCommentResponse;
import lombok.AllArgsConstructor;
import org.marketplace.comment.client.CommentServiceClient;
import org.marketplace.comment.model.request.CommentRequest;
import org.marketplace.comment.model.utils.Order;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class CommentService {
  private static final String INCORRECT_USER_ID = "incorrect user ID";
  private static final String INCORRECT_PRODUCT_ID = "incorrect product ID";
  private static final String EMPTY_TEXT = "text must be not empty";
  private static final String NOT_FOUND_COMMENT = "comment not found";


  private final CommentServiceClient client;


  public GetCommentListResponse getCommentsByUser(Integer userId, int limit, int offset, Order order) {
    var responce = client.comment().getCommentsByUser(userId, limit, offset, order);

    if (responce.getDescription().equals(INCORRECT_USER_ID)) {
      return new CommentListError(INCORRECT_USER_ID);
    } else
      return new GetCommentListResponse(
        responce.getStatus(),
        responce.getDescription(),
        responce.isHasNext(),
        responce.getComments()
      );
  }

  public GetCommentListResponse getCommentsByProduct(int productId, int limit, int offset, Order order) {
    var responce = client.comment().getCommentsByProduct(productId, limit, offset, order);
    if (responce.getDescription().equals(INCORRECT_PRODUCT_ID)) {
      return new CommentListError(INCORRECT_PRODUCT_ID);
    } else
      return new GetCommentListResponse(
        responce.getStatus(),
        responce.getDescription(),
        responce.isHasNext(),
        responce.getComments()
      );
  }

  public SaveCommentResponse saveComment(Integer userId, CommentRequest commentRequest) {
    var responce = client.comment().saveComment(
      new CommentRequest(commentRequest.getProductId(),
        userId,
        commentRequest.getText()));

    if (responce.getDescription().equals(INCORRECT_USER_ID)) {
      return new SaveCommentError(INCORRECT_USER_ID);
    }
    if (responce.getDescription().equals(INCORRECT_PRODUCT_ID)) {
      return new SaveCommentError(INCORRECT_PRODUCT_ID);
    }
    if (responce.getDescription().equals(EMPTY_TEXT)) {
      return new SaveCommentError(EMPTY_TEXT);
    } else
      return new SaveCommentResponse(
        responce.getStatus(),
        responce.getDescription(),
        responce.getComment()
      );
  }

  public DeleteCommentResponse deleteCommentById(Integer id, Integer productId, Integer userId) {

      var responce = client.comment().deleteById(id, productId, userId);
      if (responce.getDescription().equals(NOT_FOUND_COMMENT)) {
        return new DeleteCommentError(NOT_FOUND_COMMENT);
      } else
        return new DeleteCommentResponse(responce.getStatus(), responce.getDescription());
    }
}
