package org.marketplace.view.products.product;

import org.marketplace.view.products.model.response.GetProductByIdResponse;
import org.marketplace.view.products.model.response.GetProductsByCategoryResponse;
import org.marketplace.view.products.model.response.GetProductsListResponse;
import lombok.AllArgsConstructor;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.stereotype.Controller;

@Controller
@AllArgsConstructor
public class ProductController {

  private final ProductService productService;

  @QueryMapping
  public GetProductByIdResponse getProductById(@Argument Integer id) {
    return productService.getProductById(id);
  }

  @QueryMapping
  public GetProductsListResponse getAllProducts(@Argument int limit,
                                                @Argument int offset) {
    return productService.getAllProducts(limit, offset);
  }

  @QueryMapping
  public GetProductsByCategoryResponse getProductsByCategory(@Argument Integer categoryId,
                                                             @Argument int limit,
                                                             @Argument int offset) {
    return productService.getProductsByCategory(categoryId, limit, offset);
  }

}
