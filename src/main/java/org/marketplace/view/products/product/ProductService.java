package org.marketplace.view.products.product;

import org.marketplace.view.products.model.errors.*;
import org.marketplace.view.products.model.response.*;
import lombok.AllArgsConstructor;
import org.marketplace.product.client.ProductServiceClient;
import org.marketplace.product.model.utils.Order;
import org.marketplace.view.products.model.errors.ProductByIdError;
import org.marketplace.view.products.model.errors.ProductsByCategoryError;
import org.marketplace.view.products.model.errors.ProductsListError;
import org.marketplace.view.products.model.response.GetProductByIdResponse;
import org.marketplace.view.products.model.response.GetProductsByCategoryResponse;
import org.marketplace.view.products.model.response.GetProductsListResponse;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class ProductService {
  private final ProductServiceClient client;
  public static final String INCORRECT_ID = "incorrect id";
  public static final String EMPTY_PRODUCT_LIST = "product list is empty";
  public static final String INCORRECT_CATEGORY_ID = "incorrect category ID";
  public static final String EMPTY_PRODUCT = "product name must be not empty";
  public static final String EMPTY_DESCRIPTION = "description must be not empty";
  public static final String PRODUCT_NOT_FOUND = "product not found";
  public static final String ERROR = "error";

  public GetProductByIdResponse getProductById(Integer id) {
    var responce = client.product().getProductById(id);
    if (responce.getProduct() == null) {
      return new ProductByIdError(INCORRECT_ID);
    } else
      return new GetProductByIdResponse(
        responce.getStatus(),
        responce.getDescription(),
        responce.getProduct());
  }

  public GetProductsListResponse getAllProducts(int limit, int offset) {
    var responce = client.product().getAllProducts(limit, offset, Order.DESC);
    if (responce.getProducts() == null) {
      return new ProductsListError(EMPTY_PRODUCT_LIST);
    } else
      return new GetProductsListResponse(
        responce.getStatus(),
        responce.getDescription(),
        responce.isHasNext(),
        responce.getProducts());
  }

  public GetProductsByCategoryResponse getProductsByCategory(Integer categoryId, int limit, int offset) {
    var responce = client.product().getProductsByCategory(categoryId, limit, offset, Order.DESC);
    if (responce.getDescription().equals(INCORRECT_CATEGORY_ID)) {
      return new ProductsByCategoryError(INCORRECT_CATEGORY_ID);
    } else
      return new GetProductsByCategoryResponse(
        responce.getStatus(),
        responce.getDescription(),
        responce.isHasNext(),
        responce.getProducts()
      );
  }

}
