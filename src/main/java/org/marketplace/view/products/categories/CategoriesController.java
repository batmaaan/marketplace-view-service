package org.marketplace.view.products.categories;

import org.marketplace.view.products.model.response.GetAllCategoriesResponse;
import org.marketplace.view.products.model.response.GetCategoryByIdResponse;
import lombok.AllArgsConstructor;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.stereotype.Controller;

@Controller
@AllArgsConstructor
public class CategoriesController {

  private final CategoriesService categoriesService;

  @QueryMapping
  public GetCategoryByIdResponse getCategoryById(@Argument Integer id) {
    return categoriesService.getCategoryById(id);
  }

  @QueryMapping
  public GetAllCategoriesResponse getAllCategories() {
    return categoriesService.getAllCategories();
  }

}
