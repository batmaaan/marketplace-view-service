package org.marketplace.view.products.categories;

import org.marketplace.view.products.model.errors.CategoriesListError;
import org.marketplace.view.products.model.errors.GetCategoryByIdError;
import org.marketplace.view.products.model.response.GetAllCategoriesResponse;
import org.marketplace.view.products.model.response.GetCategoryByIdResponse;
import lombok.AllArgsConstructor;
import org.marketplace.product.client.ProductServiceClient;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class CategoriesService {

  private final ProductServiceClient client;

  public GetCategoryByIdResponse getCategoryById(Integer id) {
    var response = client.category().getCategoryById(id);
    if (response.getCategory().getId() == null) {
      return new GetCategoryByIdError("Category not found!");
    } else
      return new GetCategoryByIdResponse(response.getStatus(), response.getDescription(), response.getCategory());
  }

  public GetAllCategoriesResponse getAllCategories() {
    var response = client.category().getAllCategories();
    if (response.getCategories() == null) {
      return new CategoriesListError("Categories not found");
    } else return new GetAllCategoriesResponse(response.getStatus(), response.getCategories());
  }
}


