package org.marketplace.view.products.model.errors;

import org.marketplace.view.products.model.response.GetAllCategoriesResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CategoriesListError extends GetAllCategoriesResponse {
  private String message;
}
