package org.marketplace.view.products.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.marketplace.product.model.Product;
import org.marketplace.product.model.response.ProductsListResponse;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GetProductsListResponse extends ProductsListResponse {
  private String status;
  private String description;
  private boolean hasNext;
  private List<Product> products;
}
