package org.marketplace.view.products.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.marketplace.product.model.Category;
import org.marketplace.product.model.response.CategoryResponse;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GetCategoryByIdResponse extends CategoryResponse {
  private String status;
  private String description;
  private Category category;
}
