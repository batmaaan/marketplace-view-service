package org.marketplace.view.products.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.marketplace.product.model.Product;
import org.marketplace.product.model.response.ProductResponse;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GetProductByIdResponse extends ProductResponse {
  private String status;
  private String description;
  private Product product;
}
