package org.marketplace.view.products.model.errors;

import org.marketplace.view.products.model.response.SaveProductResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SaveProductError extends SaveProductResponse {
 private String message;
}
