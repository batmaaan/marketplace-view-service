package org.marketplace.view.products.model.errors;

import org.marketplace.view.products.model.response.GetProductsByCategoryResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductsByCategoryError extends GetProductsByCategoryResponse {
  private String message;
}
