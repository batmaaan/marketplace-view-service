package org.marketplace.view.products.model.errors;

import org.marketplace.view.products.model.response.GetCategoryByIdResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GetCategoryByIdError extends GetCategoryByIdResponse {
  private String message;

}
