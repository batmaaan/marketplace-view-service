package org.marketplace.view.products.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.marketplace.product.model.Category;
import org.marketplace.product.model.response.CategoriesListResponse;

import java.util.List;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class GetAllCategoriesResponse extends CategoriesListResponse {
  private String status;
  private List<Category> categories;
}
