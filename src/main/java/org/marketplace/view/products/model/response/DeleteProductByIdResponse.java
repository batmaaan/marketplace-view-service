package org.marketplace.view.products.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.marketplace.product.model.response.DeleteResponse;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DeleteProductByIdResponse extends DeleteResponse {
  private String status;
  private String description;
}
