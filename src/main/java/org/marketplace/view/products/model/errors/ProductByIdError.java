package org.marketplace.view.products.model.errors;

import org.marketplace.view.products.model.response.GetProductByIdResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductByIdError extends GetProductByIdResponse {
  private String message;
}
