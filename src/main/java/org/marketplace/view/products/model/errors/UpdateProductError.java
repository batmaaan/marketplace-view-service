package org.marketplace.view.products.model.errors;

import org.marketplace.view.products.model.response.UpdateProductResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UpdateProductError extends UpdateProductResponse {
 private String message;
}
