package org.marketplace.view.products.model.errors;

import org.marketplace.view.products.model.response.DeleteProductByIdResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DeleteProductByIdError extends DeleteProductByIdResponse {
  private String message;
}
