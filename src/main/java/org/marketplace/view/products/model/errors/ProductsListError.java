package org.marketplace.view.products.model.errors;

import org.marketplace.view.products.model.response.GetProductsListResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductsListError extends GetProductsListResponse {
 private String message;
}
