package org.marketplace.view.likes.like;

import org.marketplace.view.likes.model.LikeRequest;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.marketplace.like.client.LikeServiceClient;
import org.marketplace.like.model.Like;
import org.marketplace.like.model.LikeByProductRequest;
import org.marketplace.like.model.response.LikeResponse;
import org.marketplace.like.model.response.ProductLikesResponse;
import org.springframework.stereotype.Service;

import java.util.concurrent.CompletableFuture;

@Service
@Slf4j
@AllArgsConstructor
public class LikeService {
  private final LikeServiceClient client;

  @NotNull
  public CompletableFuture<LikeResponse> likeUnlike(Integer userId, LikeRequest likeRequest) {
    var like = new Like(userId, likeRequest.getProductId());
    return client.likeUnlike(like);
  }

  public CompletableFuture<ProductLikesResponse> likesByProduct(LikeByProductRequest request) {
    return client.likeById(request);
  }
}
