package org.marketplace.view.likes.like;

import org.marketplace.view.jwt.AppUserDetails;
import org.marketplace.view.likes.model.LikeRequest;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.marketplace.like.model.LikeByProductRequest;
import org.marketplace.like.model.response.LikeResponse;
import org.marketplace.like.model.response.ProductLikesResponse;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;

import java.util.concurrent.CompletableFuture;

@Slf4j
@Controller
@AllArgsConstructor
public class LikeController {

  private final LikeService likeService;

  @MutationMapping
  public CompletableFuture<LikeResponse> likeUnlike(@AuthenticationPrincipal AppUserDetails user,
                                                    @Argument LikeRequest likeRequest) {
     return likeService.likeUnlike(user.getUser().getId(), likeRequest);
  }
  @MutationMapping
  public  CompletableFuture<ProductLikesResponse> likesByProduct(@Argument LikeByProductRequest request) {
    return likeService.likesByProduct(request);
  }
}
