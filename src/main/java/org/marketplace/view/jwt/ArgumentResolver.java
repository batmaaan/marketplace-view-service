package org.marketplace.view.jwt;

import lombok.AllArgsConstructor;
import org.marketplace.view.utils.ApiUtil;
import org.springframework.graphql.server.WebGraphQlInterceptor;
import org.springframework.graphql.server.WebGraphQlRequest;
import org.springframework.graphql.server.WebGraphQlResponse;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.util.Collections;
import java.util.List;


@Component
@AllArgsConstructor
public class ArgumentResolver implements WebGraphQlInterceptor {
  private final JwtService jwtService;


  @Override
  public Mono<WebGraphQlResponse> intercept(WebGraphQlRequest request, Chain chain) {
    if (request.getHeaders().get(ApiUtil.AUTHORIZATION) == null) {
      return chain.next(request);
    }
    List<String> values = request.getHeaders().get(ApiUtil.AUTHORIZATION);

    if (values != null && !values.isEmpty()) {
      String token = values.get(0);
      var user = jwtService.decode(token);
      SecurityContextHolder.getContext().setAuthentication(new AppUserDetails(user));
    }
    request.configureExecutionInput(((executionInput, builder) ->
      builder.graphQLContext(Collections.singletonMap("headerName", values)).build()));
    return chain.next(request);


  }


}


