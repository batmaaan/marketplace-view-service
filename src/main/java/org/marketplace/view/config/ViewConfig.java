package org.marketplace.view.config;

import org.marketplace.comment.client.CommentServiceClient;
import org.marketplace.comment.client.CommentServiceClientImpl;
import org.marketplace.like.client.LikeServiceClient;
import org.marketplace.like.client.LikeServiceClientImpl;
import org.marketplace.product.client.ProductServiceClient;
import org.marketplace.product.client.ProductServiceClientImpl;
import org.marketplace.user.client.UserServiceClient;
import org.marketplace.user.client.UserServiceClientImpl;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ViewConfig {
  @Value("${user.service.url}")
  private String urlUser;
  @Value("${product.service.url}")
  private String urlProduct;
  @Value("${comment.service.url}")
  private String urlComment;

  @Value("${like.service.url}")
  private String urlLike;


  @Bean
  public UserServiceClient userServiceClient() {
    return new UserServiceClientImpl(urlUser);
  }

  @Bean
  public ProductServiceClient productServiceClient() {
    return new ProductServiceClientImpl(urlProduct);
  }

  @Bean
  public CommentServiceClient commentServiceClient() {
    return new CommentServiceClientImpl(urlComment);
  }

  @Bean
  public LikeServiceClient likeServiceClient() {
    return new LikeServiceClientImpl(urlLike);
  }



}
