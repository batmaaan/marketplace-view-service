package org.marketplace.view.user.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.marketplace.user.model.response.RegistrationResponse;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserRegistrationResponse extends RegistrationResponse {

  private String status;
  private String description;
  private String token;
}

