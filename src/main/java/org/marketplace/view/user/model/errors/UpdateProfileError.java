package org.marketplace.view.user.model.errors;

import org.marketplace.view.user.model.response.UpdateProfileResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UpdateProfileError extends UpdateProfileResponse {
    private String message;
}
