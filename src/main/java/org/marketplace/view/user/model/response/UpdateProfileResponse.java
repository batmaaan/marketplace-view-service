package org.marketplace.view.user.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.marketplace.user.model.response.InformationProfileResponse;
import org.marketplace.user.model.response.ProfileResponse;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UpdateProfileResponse extends InformationProfileResponse {
  private String status;
  private String description;
  private ProfileResponse result;
}
