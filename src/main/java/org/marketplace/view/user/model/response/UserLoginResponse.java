package org.marketplace.view.user.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.marketplace.user.model.response.InformationResponse;
import org.marketplace.user.model.response.UserResponse;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserLoginResponse extends InformationResponse {
  private String status;
  private String description;
  private String token;
  private UserResponse result;

}
