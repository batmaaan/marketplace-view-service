package org.marketplace.view.user.model.errors;

import org.marketplace.view.user.model.response.GetProfileResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GetProfileError extends GetProfileResponse {
    private String message;
}
