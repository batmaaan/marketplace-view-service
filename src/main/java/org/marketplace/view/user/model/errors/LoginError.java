package org.marketplace.view.user.model.errors;

import org.marketplace.view.user.model.response.UserLoginResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoginError extends UserLoginResponse {
    private String message;
}
