package org.marketplace.view.user.model.errors;

import org.marketplace.view.user.model.response.UserRegistrationResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RegistrationError extends UserRegistrationResponse {
    private String message;
}
