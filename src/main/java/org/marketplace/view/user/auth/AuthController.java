package org.marketplace.view.user.auth;

import org.marketplace.view.user.model.response.UserLoginResponse;
import lombok.AllArgsConstructor;
import org.marketplace.user.model.request.UserLoginRequest;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.stereotype.Controller;


@Controller
@AllArgsConstructor
public class AuthController {
  private final  AuthService authService;


  @MutationMapping
  UserLoginResponse login(@Argument UserLoginRequest loginRequest) {
    return authService.login(loginRequest);

  }
}
