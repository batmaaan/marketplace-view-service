package org.marketplace.view.user.auth;

import org.marketplace.view.jwt.JwtService;
import org.marketplace.view.user.model.errors.LoginError;
import org.marketplace.view.user.model.response.UserLoginResponse;
import lombok.AllArgsConstructor;
import org.marketplace.user.client.UserServiceClient;
import org.marketplace.user.model.User;
import org.marketplace.user.model.request.UserLoginRequest;

import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class AuthService {

  private final UserServiceClient userServiceClient;
  private final JwtService jwtService;

  public UserLoginResponse login(UserLoginRequest loginRequest) {
    var response = userServiceClient.auth().login(loginRequest);


    if (loginRequest.getEmail() == null) {
      return new LoginError("Email must be not null");
    }
    if (loginRequest.getPassword() == null) {
      return new LoginError("Password must be not null");
    }
    if (response.getDescription().equals("user not found")) {
      return new LoginError("User not found");
    } else {
       User user = userServiceClient.user().getUserByEmail(loginRequest.getEmail());
      String token = jwtService.encode(user);

      return new UserLoginResponse(response.getStatus(), response.getDescription(),token, response.getResult());
    }
  }
}
