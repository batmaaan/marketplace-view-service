package org.marketplace.view.user.profile;

import org.marketplace.view.user.model.errors.GetProfileError;
import org.marketplace.view.user.model.errors.UpdateProfileError;
import org.marketplace.view.user.model.response.GetProfileResponse;
import org.marketplace.view.user.model.response.UpdateProfileResponse;
import lombok.AllArgsConstructor;
import org.marketplace.user.client.UserServiceClient;
import org.marketplace.user.model.request.UpdateProfileRequest;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
@AllArgsConstructor
public class ProfileService {
  private final UserServiceClient userServiceClient;

  public GetProfileResponse getProfile(Integer userId) {
    var response = userServiceClient.profile().getProfile(userId);
    if (response.getResult().getUserId() == null) {
      return new GetProfileError("Profile not found!");
    } else
      return new GetProfileResponse(response.getStatus(), response.getDescription(), response.getResult());
  }

  public UpdateProfileResponse updateProfile(UpdateProfileRequest profileRequest) {
    var response = userServiceClient.profile().updateProfile(profileRequest);
    if (profileRequest.getUserId() == null) {
      return new UpdateProfileError("User ID must be not empty");
    }
    if (profileRequest.getFirstName() == null) {
      return new UpdateProfileError("First name must be not empty");
    }
    if (profileRequest.getLastName() == null) {
      return new UpdateProfileError("Last name must be not empty");
    }
    if (!Objects.equals(response.getStatus(), "ok")) {
      return new UpdateProfileError("Something wrong!");
    }
    return new UpdateProfileResponse(response.getStatus(), response.getDescription(), response.getResult());
  }
}
