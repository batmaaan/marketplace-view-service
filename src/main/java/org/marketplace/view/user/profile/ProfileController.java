package org.marketplace.view.user.profile;

import org.marketplace.view.user.model.response.GetProfileResponse;
import org.marketplace.view.user.model.response.UpdateProfileResponse;
import lombok.AllArgsConstructor;
import org.marketplace.user.model.request.UpdateProfileRequest;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.stereotype.Controller;

@Controller
@AllArgsConstructor
public class ProfileController {

  private final ProfileService profileService;

  @QueryMapping
  public GetProfileResponse getProfile(@Argument Integer userId) {
    return profileService.getProfile(userId);
  }

  @MutationMapping
  public UpdateProfileResponse updateProfile(@Argument UpdateProfileRequest profileRequest) {
    return profileService.updateProfile(profileRequest);


  }
}
