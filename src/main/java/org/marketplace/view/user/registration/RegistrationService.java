package org.marketplace.view.user.registration;

import lombok.AllArgsConstructor;
import org.marketplace.user.client.UserServiceClient;
import org.marketplace.user.model.request.UserRegistrationRequest;
import org.marketplace.view.jwt.JwtService;
import org.marketplace.view.user.model.errors.RegistrationError;
import org.marketplace.view.user.model.response.UserRegistrationResponse;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class RegistrationService {
  private final UserServiceClient userServiceClient;

  private final JwtService jwtService;

  public UserRegistrationResponse registration(UserRegistrationRequest registrationRequest) {
    var response = userServiceClient.registration().registration(registrationRequest);

    if ("error".equals(response.getStatus())) {
      return new RegistrationError(response.getDescription());
    } else {

      String token = jwtService.encode(response.getUser());
      return new UserRegistrationResponse(response.getStatus(), response.getDescription(), token);
    }
  }
}
