package org.marketplace.view.user.registration;

import org.marketplace.view.user.model.response.UserRegistrationResponse;
import lombok.AllArgsConstructor;
import org.marketplace.user.model.request.UserRegistrationRequest;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.stereotype.Controller;

@Controller
@AllArgsConstructor
public class RegistrationController {
  private final RegistrationService registrationService;
    @MutationMapping
    UserRegistrationResponse registration(@Argument UserRegistrationRequest registrationRequest) {
    return registrationService.registration(registrationRequest);
  }
}
