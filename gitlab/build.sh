mvn clean compile deploy
mvn compile -U com.google.cloud.tools:jib-maven-plugin:3.2.1:build -Djib.to.auth.username=$DOCKER_USERNAME -Djib.to.auth.password=$DOCKER_PASS  -Dimage=batmaaan/marketplace-view-service:0.0.1-SNAPSHOT
cd /home/finch/app/view; docker-compose pull; docker-compose stop; docker-compose up -d;
